package TextParts;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Sentence {

    private List<String> listOfSentences;

    public Sentence() {
        this.listOfSentences = new ArrayList<>();
    }

    public void addSentencesToTheList(String sentences) {
        this.listOfSentences.add(sentences);
    }

    public void printListOfSentences() {
        for (String sentence : this.listOfSentences) {
            System.out.println(sentence);
        }
    }

    public void sortSentencesByLength() {
        this.listOfSentences.sort(new Comparator<String>() {
                                  @Override
                                  public int compare(String s1, String s2) {
                                     return s1.length() - s2.length();
                                  }});
    }

    @Override
    public String toString() {
        return "Sentence{" + listOfSentences +'}';
    }
}
