package TextParts;

import java.util.ArrayList;
import java.util.List;

public class Paragraph {

    private List<String> listOfParagraphs;

    public Paragraph() {
        this.listOfParagraphs = new ArrayList<>();
    }

    public void addParagraphsToTheList(String paragraph) {
        this.listOfParagraphs.add(paragraph);
    }

    public void printListOfParagraphs() {
        for (String paragraph : this.listOfParagraphs) {
            System.out.println(paragraph);
        }
    }

    @Override
    public String toString() {
        return "Paragraph{" + listOfParagraphs +'}';
    }
}
