package TextParts;

import java.util.ArrayList;
import java.util.List;

public class Word {

    private List<String> listOfWords;

    public Word() {
        this.listOfWords = new ArrayList<>();
    }

    public void addWordsToTheList(String word) {
        this.listOfWords.add(word);
    }

    public void printListOfWords() {
        for (String word : this.listOfWords) {
            System.out.println(word);
        }
    }

    @Override
    public String toString() {
        return "Word{" + listOfWords +'}';
    }
}
