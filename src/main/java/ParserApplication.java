import Parsers.ParagraphParser;
import Parsers.SentenceParser;
import Parsers.TextParser;
import Parsers.WordParser;

public class ParserApplication {

    private static String text =
            "  It has survived not only five centuries, but also the leap into electronic typesetting, remaining " +
            "essentially unchanged. It was popularised in the with the release of Letraset sheets containing " +
            "Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker " +
            "including versions of Lorem Ipsum.\n" +
            "  It is a long established fact that a reader will be distracted by the readable content of a page " +
            "when looking at its layout. The point of using Ipsum is that it has a more-or-less normal distribution " +
            "of letters, as opposed to using 'Content here, content here', making it look like readable English.\n" +
            "  It is an established fact that a reader will be of a page when looking at its layout.\n" +
            "  Bye.";

    public static void main(String[] args) {
        TextParser wordParser = new WordParser();
        wordParser.parseText(text);
        TextParser sentenceParser = new SentenceParser();
        sentenceParser.parseText(text);
        TextParser paragraphParser = new ParagraphParser();
        paragraphParser.parseText(text);
    }
}
