package Parsers;

import TextParts.Paragraph;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser implements TextParser {

    private String paragraphRegEx = "^(?:(?!^$)[\\s\\S])+$";
    private Paragraph paragraph;

    public ParagraphParser() {
        paragraph = new Paragraph();
    }

    @Override
    public void parseText(String text) {
        Matcher matcher = Pattern.compile(paragraphRegEx).matcher(text);
        while (matcher.find()) {
            paragraph.addParagraphsToTheList(matcher.group());
        }
        paragraph.printListOfParagraphs();
    }
}
