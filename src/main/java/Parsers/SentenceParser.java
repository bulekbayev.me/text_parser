package Parsers;

import TextParts.Sentence;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser implements TextParser {

    private String sentenceRegEx = "((\\d+\\.\\s*)*[A-Z]([^?!.\\(]|\\([^\\)]*\\))*[.?!])";
    private Sentence sentence;

    public SentenceParser() {
        sentence = new Sentence();
    }

    @Override
    public void parseText(String text) {
        Matcher matcher = Pattern.compile(sentenceRegEx).matcher(text);
        while (matcher.find()) {
            sentence.addSentencesToTheList(matcher.group(1));
        }
        sentence.printListOfSentences();
    }
}
