package Parsers;

public interface TextParser {

    void parseText(String text);
}
