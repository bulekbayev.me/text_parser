package Parsers;

import TextParts.Word;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser implements TextParser {

    private String wordRegEx = "([a-zA-z&&[^0-9]])+";
    private Word word;

    public WordParser() {
        word = new Word();
    }

    @Override
    public void parseText(String text) {
        Matcher matcher = Pattern.compile(wordRegEx).matcher(text);
        while (matcher.find()) {
            word.addWordsToTheList(matcher.group());
        }
        word.printListOfWords();
    }
}
