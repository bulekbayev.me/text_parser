## Text parser

## Общие требования
1. Код приложения должен быть отформатирован в едином стиле и соответствовать Java Code Convention.
2. Приложение должно быть работоспособным - т.е. запускаться без дополнительных манипуляций.
3. Для записи логов использовать Log4J2 (используйте maven для управлением зависимостями).

## Задание
Cоздать приложение, разбирающее текст из файла и позволяющее выполнять с текстом следующие операции: 

1. Отсортировать абзацы по количеству предложений.
2. Отсортироватьслова впредложении по длине. 
3. Отсортироватьпредложения в абзаце по количеству слов.

Текст:
```text
  It has survived not only five centuries, but also the leap into electronic typesetting, remaining 
essentially unchanged. It was popularised in the with the release of Letraset sheets containing 
Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker 
including versions of Lorem Ipsum.
  It is a long established fact that a reader will be distracted by the readable content of a page 
when looking at its layout. The point of using Ipsum is that it has a more-or-less normal distribution 
of letters, as opposed to using 'Content here, content here', making it look like readable English.
  It is an established fact that a reader will be of a page when looking at its layout.
  Bye.
```

Требования: 
 
1. Разобранный текст должен быть представлен в виде объекта, содержащего, например, абзацы, 
предложения, лексемы, слова, символы. Лексема – часть текста, ограниченная пробельными символами.
Для построения вышеописанной структуры использовать паттерн Composite.
2. Классы описывающие структуру текста не должны быть перенагружены методами логики.
3. Разобранный текст необходимо восстановить в первоначальном виде. Пробелы и знаки табуляции при 
разборе могут заменяться одним пробелом.
4. Для деления текста на составляющие можно использовать регулярные выражения.
5. Код, выполняющий разбиение текста на составляющие части, следует оформить в виде классов-парсеров 
с использованием паттерна Chain of Responsibility. 
6. При разработке парсеров, разбирающих текст, необходимо следить за количеством создаваемых парсеров. 
 